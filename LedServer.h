#ifndef LED_SERVER_H
#define LED_SERVER_H

#include <string>
#include "File.h"

class LedServer
{
   enum class LedColor { Red, Green, Blue };

   struct ProtoEntry
   {
      std::string command;
      std::string (LedServer::*parseFunc)(const std::string& s, int paramIndex);
   };

   static const std::string OkReply, FailedReply;

   static ProtoEntry m_protoEntries[6];

   bool m_on = true;
   LedColor m_color = LedColor::Red;
   int m_rate = 0;

   static std::string makeReply(const std::string& value) { return "OK " + value + "\n"; }
   static File openNewFifo(const std::string& path, int flags);
   const char* colorStr() const;

   std::string setLedState(const std::string& s, int paramIndex);
   std::string getLedState(const std::string& s, int paramIndex);
   std::string setLedColor(const std::string& s, int paramIndex);
   std::string getLedColor(const std::string& s, int paramIndex);
   std::string setLedRate(const std::string& s, int paramIndex);
   std::string getLedRate(const std::string& s, int paramIndex);

public:
   int run();
};

#endif
