#ifndef FILE_H
#define FILE_H

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

class Exception : public std::exception
{
   std::string m_message;
   int m_error = -1;

public:
   Exception(){}
   Exception(const std::string& msg, int error) : m_message(msg), m_error(error){}

   const char* what() const noexcept override { return m_message.c_str(); }
   const std::string& message() const { return m_message; }
   int error() const { return m_error; }
};

class File
{
   int m_fd = -1;

public:
   File(){}
   File(File&& f) : m_fd(f.m_fd){ f.m_fd = -1; }
   File(const std::string& path, int flags) { open(path, flags); }
   virtual ~File()
   {
      if (m_fd >= 0)
         ::close(m_fd);
   }

   int fd() const { return m_fd; }

   void open(const std::string& path, int flags)
   {
      if (m_fd >= 0)
         ::close(m_fd);

      m_fd = ::open(path.c_str(), flags);
      if (m_fd < 0)
         throw Exception(std::string("Can't create file \"") + path + '"', errno);
   }

   void close()
   {
      if (m_fd < 0) return;
      ::close(m_fd);
      m_fd = -1;
   }

   static void unlink(const std::string& path)
   {
      if (::unlink(path.c_str()) < 0 && errno != ENOENT)
         throw Exception("Can't remove file \"" + path + '"', errno);
   }

   static void chmod(const std::string& path, mode_t mode)
   {
      if (::chmod(path.c_str(), mode) < 0)
         throw Exception("Can't chmod of file \"" + path + '"', errno);
   }

   static void makeFifo(const std::string& path, mode_t mode)
   {
      if (mkfifo(path.c_str(), mode) < 0)
         throw Exception("Can't make fifo \"" + path + '"', errno);
   }

   ssize_t read(void* buf, int n) const
   {
      ssize_t r = ::read(m_fd, buf, n);
      if (r < 0)
         throw Exception("Error reading file", errno);
      return r;
   }

   void write(const void* buf, ssize_t n) const
   {
      ssize_t written = ::write(m_fd, buf, n);
      if (written < 0)
         throw Exception("Error writing to file", errno);
      if (n != written)
         throw Exception("Error writing to file", -1);
   }
};

#endif
