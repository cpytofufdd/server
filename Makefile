out=LedServer

all:
	@g++ -std=c++11 -O2 -Wall -Wextra -Werror=return-type -DNDEBUG LedServer.cpp -o $(out)

clean:
	@rm -rf $(out) &> /dev/null
