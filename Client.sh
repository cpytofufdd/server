#!/bin/bash

declare -A commands
commands[seton]="set-led-state on"
commands[setoff]="set-led-state off"
commands[ison]="get-led-state"
commands[setred]="set-led-color red"
commands[setgreen]="set-led-color green"
commands[setblue]="set-led-color blue"
commands[color]="get-led-color"
commands[setrate]="set-led-rate %s"
commands[getrate]="get-led-rate"

function showUsage()
{
   echo -n "Usage: `basename $0` "
   for k in ${!commands[@]}; do
      echo -n "$k|"
   done
   echo
   exit
}

[ -n "$1" ] || showUsage

command=${commands[$1]}
[ -n "$command" ] || showUsage

{ [[ "$command" == *%* ]] && printf "$command\n" $2 || echo "$command"; } > /tmp/toserver
cat /tmp/toclient
