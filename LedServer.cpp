#include <iostream>
#include "File.h"
#include "LedServer.h"
#include <string.h>

using std::cout; using std::endl;

int main(int /*argc*/, char** /*argv*/) { return LedServer().run(); }

const std::string LedServer::OkReply = "OK\n", LedServer::FailedReply = "FAILED\n";

LedServer::ProtoEntry LedServer::m_protoEntries[6] =
{
   { "set-led-state ", &LedServer::setLedState },
   { "get-led-state\n", &LedServer::getLedState },
   { "set-led-color ", &LedServer::setLedColor },
   { "get-led-color\n", &LedServer::getLedColor },
   { "set-led-rate ", &LedServer::setLedRate },
   { "get-led-rate\n", &LedServer::getLedRate }
};

std::string LedServer::setLedState(const std::string& s, int paramIndex)
{
   if (0 == strncmp(&s[paramIndex], "on\n", 3))
   {
      m_on = true;
      return OkReply;
   }
   else if (0 == strncmp(&s[paramIndex], "off\n", 4))
   {
      m_on = false;
      return OkReply;
   }
   return FailedReply;
}

std::string LedServer::getLedState(const std::string& /*s*/, int /*paramIndex*/)
{
   return makeReply(m_on ? "on" : "off");
}

std::string LedServer::setLedColor(const std::string& s, int paramIndex)
{
   if (0 == strncmp(&s[paramIndex], "red\n", 4))
   {
      m_color = LedColor::Red;
      return OkReply;
   }
   else if (0 == strncmp(&s[paramIndex], "green\n", 6))
   {
      m_color = LedColor::Green;
      return OkReply;
   }
   else if (0 == strncmp(&s[paramIndex], "blue\n", 5))
   {
      m_color = LedColor::Blue;
      return OkReply;
   }
   return FailedReply;
}

std::string LedServer::getLedColor(const std::string& /*s*/, int /*paramIndex*/)
{
   std::string color;
   switch(m_color)
   {
   case LedColor::Red: color = "red"; break;
   case LedColor::Green: color = "green"; break;
   case LedColor::Blue: color = "blue"; break;
   }
   return makeReply(color);
}

std::string LedServer::setLedRate(const std::string& s, int paramIndex)
{
   if (s[paramIndex + 1] == '\n')
   {
      int n = s[paramIndex] - '0';
      if (n >= 0 && n <= 5)
      {
         m_rate = n;
         return OkReply;
      }
   }
   return FailedReply;
}

std::string LedServer::getLedRate(const std::string& /*s*/, int /*paramIndex*/)
{
   return makeReply(std::to_string(m_rate));
}

const char* LedServer::colorStr() const
{
   if (m_on)
      switch(m_color)
      {
      case LedColor::Red: return "\E[01;31m";
      case LedColor::Green: return "\E[01;32m";
      case LedColor::Blue: return "\E[01;34m";
      }
   return "\E[00;0m";
}

int LedServer::run()
{
   std::string toServerFifoName = "/tmp/toserver", serverToClientFifoName = "/tmp/toclient";

   try
   {
      File toServer = openNewFifo(toServerFifoName, O_RDWR);

      std::string buf;
      buf.resize(1000);
      while(1)
      {
         cout << "read/wait" << endl;

         File toClient = openNewFifo(serverToClientFifoName, O_WRONLY);

         int n = toServer.read(&buf[0], buf.size());
         if (!n) continue;
         //cout << "received bytes: " << n << ' ' << std::string(&buf[0], n - 1) << ' ' << rand() << endl;

         std::string reply;
         for(const ProtoEntry& e : m_protoEntries)
         {
            if (0 == strncmp(&buf[0], e.command.c_str(), e.command.length()))
               reply = (this->*e.parseFunc)(buf, e.command.length());
         }
         if (reply.empty()) reply = FailedReply;

         //cout << "reply: " << reply.substr(0, reply.length() - 1) << endl;
         toClient.write(reply.c_str(), reply.length());

         cout << colorStr()
            << "on=" << (m_on ? "on" : "off")
            << " color=" << (m_color == LedColor::Red ? "red" : m_color == LedColor::Green ? "green" : "blue")
            << " rate=" << m_rate << endl;
         cout << "\E[00;0m";
      }
   }
   catch(const Exception& e)
   {
      std::cerr << e.message() << ". Error code=" << e.error() << endl;
      return 1;
   }
   catch(const std::exception& e)
   {
      std::cerr << "Unexpected error " << e.what() << endl;
      return 1;
   }
   return 0;
}

File LedServer::openNewFifo(const std::string& path, int flags)
{
   File::unlink(path);
   File::makeFifo(path.c_str(), 0666);
   File::chmod(path, 0666);
   return File(path, flags);
}
